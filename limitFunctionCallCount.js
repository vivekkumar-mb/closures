// function limitFunctionCallCount(cb, n) {
//     // Should return a function that invokes `cb`.
//     // The returned function should only allow `cb` to be invoked `n` times.
//     // Returning null is acceptable if cb can't be returned
// }
const limitFunctionCallCount = (cb, n) => {
    if (!typeof cb == 'function' || n <= 0) return null;
    let count=0;
    return (() => {
        if(count<n){
            count++;
            return cb();
        }
        else
        return null;
    });
}
module.exports = limitFunctionCallCount;