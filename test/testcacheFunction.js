
const cacheFunction = require("../cacheFunction");



const cb=(a,b)=>{
return a+b;
}
const ans=cacheFunction(cb);

console.log(ans(1,2));//3
console.log(ans(1,2));//This result is from cache :3
console.log(ans(3,2));//5
console.log(ans(3,2));//This result is from cache :5
console.log(ans(6,2));//8

