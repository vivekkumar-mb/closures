const counterFactory = require("../counterFactory");

const counterobj=counterFactory();
console.log(counterobj.increment());//1
console.log(counterobj.increment());//2
console.log(counterobj.increment());//3
console.log(counterobj.increment());//4
console.log(counterobj.decrement());//3
console.log(counterobj.decrement());//2