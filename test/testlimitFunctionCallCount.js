const limitFunctionCallCount = require('../limitFunctionCallCount.js');

const cb=()=>{
    console.log('Hello World');
}
const ans=limitFunctionCallCount(cb,5);

ans();//Hello World
ans();//Hello World
ans();//Hello World
ans();//Hello World
ans();//Hello World
ans();//no output
ans();//no output